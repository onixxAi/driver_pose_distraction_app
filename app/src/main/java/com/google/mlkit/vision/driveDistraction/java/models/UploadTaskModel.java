package com.google.mlkit.vision.driveDistraction.java.models;

import android.net.Uri;

public class UploadTaskModel {
    public Uri videoUri;
    public Uri jsonUri;
    public Uri videoSessionUri = null;
    public boolean isVideo;

    public UploadTaskModel() {

    }

    public UploadTaskModel(Uri Uri, boolean isVideo) {
        this.isVideo = isVideo;
        if (isVideo)
            this.videoUri = Uri;
        else this.jsonUri = Uri;

    }
}
