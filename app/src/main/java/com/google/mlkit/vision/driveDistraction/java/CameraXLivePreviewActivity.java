/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.mlkit.vision.driveDistraction.java;

import androidx.appcompat.app.AlertDialog;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraControl;
import androidx.camera.core.VideoCapture;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.camera.core.CameraInfoUnavailableException;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.mlkit.common.MlKitException;

import com.google.mlkit.vision.driveDistraction.BuildConfig;
import com.google.mlkit.vision.driveDistraction.CameraXViewModel;
import com.google.mlkit.vision.driveDistraction.GraphicOverlay;
import com.google.mlkit.vision.driveDistraction.ObjectSerializer;
import com.google.mlkit.vision.driveDistraction.R;
import com.google.mlkit.vision.driveDistraction.VisionImageProcessor;

import com.google.mlkit.vision.driveDistraction.java.models.EventModel;
import com.google.mlkit.vision.driveDistraction.java.models.UploadTaskModel;
import com.google.mlkit.vision.driveDistraction.java.models.UserModel;
import com.google.mlkit.vision.driveDistraction.java.posedetector.PoseDetectorProcessor;
import com.google.mlkit.vision.driveDistraction.preference.PreferenceUtils;
import com.google.mlkit.vision.driveDistraction.preference.SettingsActivity;
import com.google.mlkit.vision.pose.PoseDetectorOptionsBase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@KeepName
@RequiresApi(VERSION_CODES.LOLLIPOP)
public final class CameraXLivePreviewActivity extends AppCompatActivity
        implements OnRequestPermissionsResultCallback,
        OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {
  private static final String TAG = "CameraXLivePreview";
  private static final int PERMISSION_REQUESTS = 1;
  private static final int RC_SIGN_IN = 123;

  private static final String OBJECT_DETECTION = "Object Detection";


  private static final String STATE_SELECTED_MODEL = "selected_model";

  private PreviewView previewView;
  private GraphicOverlay graphicOverlay;

  @Nullable private ProcessCameraProvider cameraProvider;
  @Nullable private Preview previewUseCase;
  @Nullable private ImageAnalysis analysisUseCase;
  @Nullable private VideoCapture videoCapture;
  @Nullable private VisionImageProcessor imageProcessor;
  private boolean needUpdateGraphicOverlayImageSourceInfo;
  private boolean isRecording = false;
  private boolean videoIsBusy = false;

  private String selectedModel = OBJECT_DETECTION;
  private int lensFacing = CameraSelector.LENS_FACING_BACK;
  private int screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
  private CameraSelector cameraSelector;

  private String folder_main = "Driver_distraction_data";
  private CountDownTimer cycleTimer;
  private ImageView redRecCircle;
  private TextView infoLabel;
  private Button startStopRec;
  private Animation animation;

  private FirebaseAuth mAuth;
  private String license_num;
  private FirebaseUser user;
  private boolean loginPageActive = false;
  private FirebaseDatabase mDatabase;
  private FirebaseFirestore firestoredb;
  private FirebaseStorage storage;

  private ArrayList<UploadTaskModel> uploadQue;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.d(TAG, "onCreate");

    if (VERSION.SDK_INT < VERSION_CODES.LOLLIPOP) {
      Toast.makeText(
              getApplicationContext(),
              "CameraX is only supported on SDK version >=21. Current SDK version is "
                      + VERSION.SDK_INT,
              Toast.LENGTH_LONG)
              .show();
      return;
    }

    if (savedInstanceState != null) {
      selectedModel = savedInstanceState.getString(STATE_SELECTED_MODEL, OBJECT_DETECTION);
    }

    cameraSelector = new CameraSelector.Builder().requireLensFacing(lensFacing).build();

    mAuth = FirebaseAuth.getInstance();
    mDatabase = FirebaseDatabase.getInstance(getString(R.string.firebase_database_url));
    firestoredb = FirebaseFirestore.getInstance();
    storage = FirebaseStorage.getInstance();


    setContentView(R.layout.activity_vision_camerax_live_preview);
    previewView = findViewById(R.id.preview_view);
    if (previewView == null) {
      Log.d(TAG, "previewView is null");
    }
    graphicOverlay = findViewById(R.id.graphic_overlay);
    if (graphicOverlay == null) {
      Log.d(TAG, "graphicOverlay is null");
    }


    // Creating adapter for spinner

    animation = new AlphaAnimation(1, 0);
    animation.setDuration(1000); // duration - one second
    animation.setInterpolator(new LinearInterpolator());
    animation.setRepeatCount(Animation.INFINITE);
    animation.setRepeatMode(Animation.REVERSE);

    redRecCircle = findViewById(R.id.recCircle);


    ToggleButton facingSwitch = findViewById(R.id.facing_switch);
    facingSwitch.setOnCheckedChangeListener(this);

    new ViewModelProvider(this, AndroidViewModelFactory.getInstance(getApplication()))
            .get(CameraXViewModel.class)
            .getProcessCameraProvider()
            .observe(
                    this,
                    provider -> {
                      cameraProvider = provider;
                      if (allPermissionsGranted()) {
                        bindAllCameraUseCases();
                      }
                    });

    findViewById(R.id.orientation_switch).setOnClickListener(v -> {
      screenOrientation = screenOrientation ==
             ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ?
             ActivityInfo.SCREEN_ORIENTATION_PORTRAIT :
             ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
      setRequestedOrientation(screenOrientation);

    });

    infoLabel = findViewById(R.id.InfoLabel);
    TextView versionName = findViewById(R.id.versionNameText);
    versionName.setText("version: "+BuildConfig.VERSION_NAME);
    startStopRec = findViewById(R.id.startStopRecording);
    startStopRec.setOnClickListener(
            v -> {
              SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
              license_num = sharedPreferences.getString(getString(R.string.pref_key_license_plate), "");
              if (license_num.isEmpty()) {
                Toast.makeText(
                        getApplicationContext(),
                        "Перед началом записи необходимо задать номер ТС в настройках",
                        Toast.LENGTH_LONG)
                        .show();
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                intent.putExtra(
                        SettingsActivity.EXTRA_LAUNCH_SOURCE,
                        SettingsActivity.LaunchSource.CAMERAX_LIVE_PREVIEW);
                startActivity(intent);
                return;
              }
              String alert2 = "Имя: " + user.getDisplayName();
              String alert3 = "Номер ТС: " + license_num;

              if (imageProcessor != null && !isRecording) {
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Данные водителя верны?")
                        .setMessage(alert2 + "\n" + alert3)
                        .setPositiveButton("Изменить", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                            intent.putExtra(
                                    SettingsActivity.EXTRA_LAUNCH_SOURCE,
                                    SettingsActivity.LaunchSource.CAMERAX_LIVE_PREVIEW);
                            startActivity(intent);
                          }
                        })
                        .setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {
                            initRecordingTimer();
                          }
                        })
                        .create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                  private static final int AUTO_DISMISS_MILLIS = 6000;

                  @Override
                  public void onShow(final DialogInterface dialog) {
                    final Button defaultButton = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                    final CharSequence negativeButtonText = defaultButton.getText();
                    new CountDownTimer(AUTO_DISMISS_MILLIS, 100) {
                      @Override
                      public void onTick(long millisUntilFinished) {
                        defaultButton.setText(String.format(
                                Locale.getDefault(), "%s (%d)",
                                negativeButtonText,
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) + 1 //add one so it never displays zero
                        ));
                      }

                      @Override
                      public void onFinish() {
                        if (((AlertDialog) dialog).isShowing()) {
                          dialog.dismiss();
                          initRecordingTimer();
                        }
                      }
                    }.start();
                  }
                });
                dialog.show();
              } else if (isRecording) {
                stopRecordingCycle();
                startStopRec.setText(R.string.recordingStart);
                redRecCircle.clearAnimation();
                redRecCircle.setVisibility(View.INVISIBLE);
                startStopRec.setBackgroundColor(getColor(R.color.green));
              } else {
                Toast.makeText(
                        getApplicationContext(),
                        "Дождитесь запуска модели",
                        Toast.LENGTH_SHORT)
                        .show();
              }

            }
    );
    Button poseCalibrate = findViewById(R.id.poseCalibrateBtn);
    poseCalibrate.setOnClickListener(
            v -> {
              if (imageProcessor != null && !isRecording) {
                startStopRec.setEnabled(false);
                new CountDownTimer(5000, 1000) {

                  @Override
                  public void onTick(long millisUntilFinished) {
                    int time = (int) millisUntilFinished / 1000;
                    if (time == 0) {
                      infoLabel.setText("Калибровка");
                      imageProcessor.setupThreshold();
                    } else {
                      infoLabel.setText(String.valueOf(time));
                    }

                  }

                  @Override
                  public void onFinish() {
                    infoLabel.setText("");
                    startStopRec.setEnabled(true);
                  }
                }.start();

              } else {
                Toast.makeText(
                        getApplicationContext(),
                        "Дождитесь запуска модели",
                        Toast.LENGTH_SHORT)
                        .show();
              }
            }
    );
    ImageView settingsButton = findViewById(R.id.settings_button);
    settingsButton.setOnClickListener(
            v -> {
              Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
              intent.putExtra(
                      SettingsActivity.EXTRA_LAUNCH_SOURCE,
                      SettingsActivity.LaunchSource.CAMERAX_LIVE_PREVIEW);
              startActivity(intent);
            });

    if (!allPermissionsGranted()) {
      getRuntimePermissions();
    }

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    try {
      String testString = prefs.getString(getString(R.string.pref_upload_que), "nothing");
      uploadQue = (ArrayList<UploadTaskModel>) ObjectSerializer.deserialize(prefs.getString(getString(R.string.pref_upload_que), ObjectSerializer.serialize(new ArrayList<UploadTaskModel>())));
    } catch (IOException | ClassNotFoundException e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      e.printStackTrace();
    }

    if (uploadQue.size() > 0) {
      if (isNetworkAvailable(getApplicationContext())) {
        Toast.makeText(this, "Начало загрузки данных из предыдущей сессии", Toast.LENGTH_LONG
        ).show();
        processUploads();
      }
    }
  }

  private void checkLogin() {
    user = FirebaseAuth.getInstance().getCurrentUser();


    if (user == null && !loginPageActive) {
      loginPageActive = true;
      startActivityForResult(
              AuthUI.getInstance()
                      .createSignInIntentBuilder()
                      .setIsSmartLockEnabled(false)
                      .setAvailableProviders(Collections.singletonList(new AuthUI.IdpConfig.EmailBuilder().setAllowNewAccounts(false).build()))
                      .build(),
              RC_SIGN_IN
      );
    }
  }

  private void initRecordingTimer() {
    new CountDownTimer(5000, 1000) {

      //Здесь обновляем текст счетчика обратного отсчета с каждой секундой
      public void onTick(long millisUntilFinished) {
        int time = (int) millisUntilFinished / 1000;
        if (time == 0) {
          infoLabel.setText("Начало записи");
          startRecordingCycle();
          redRecCircle.setVisibility(View.VISIBLE);
          redRecCircle.startAnimation(animation);
          isRecording = true;
        } else {
          infoLabel.setText(String.valueOf(time));
        }
      }

      public void onFinish() {
        infoLabel.setText("");
      }
    }.start();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == RC_SIGN_IN) {
      IdpResponse response = IdpResponse.fromResultIntent(data);
      loginPageActive = false;

      if (resultCode == RESULT_OK) {
        // Successfully signed in
        user = FirebaseAuth.getInstance().getCurrentUser();
        user.getIdToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
          @Override
          public void onSuccess(@NonNull GetTokenResult getTokenResult) {
            String company = (String) getTokenResult.getClaims().get("company");
            boolean isAdmin = (boolean) getTokenResult.getClaims().get("admin");
            UserModel userInfo = new UserModel(user.getDisplayName(), user.getEmail(), user.getPhoneNumber(), company, isAdmin);
            updateUserRecord(userInfo, user, firestoredb);
          }
        });


      } else {
        if (response == null) {
          // User pressed back button

        } else if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
          Toast.makeText(this, "Нет подключения к интернету!", Toast.LENGTH_LONG).show();
        } else {
          FirebaseCrashlytics.getInstance().log(response.getError().getMessage());
        }
      }
    }
  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle bundle) {
    super.onSaveInstanceState(bundle);
    bundle.putString(STATE_SELECTED_MODEL, selectedModel);
  }

  @Override
  public synchronized void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
    // An item was selected. You can retrieve the selected item using
    // parent.getItemAtPosition(pos)
    selectedModel = parent.getItemAtPosition(pos).toString();
    Log.d(TAG, "Selected model: " + selectedModel);
    bindAnalysisUseCase();
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {
    // Do nothing.
  }

  @Override
  public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    if (cameraProvider == null) {
      return;
    }
    int newLensFacing =
        lensFacing == CameraSelector.LENS_FACING_FRONT
            ? CameraSelector.LENS_FACING_BACK
            : CameraSelector.LENS_FACING_FRONT;
    CameraSelector newCameraSelector =
        new CameraSelector.Builder().requireLensFacing(newLensFacing).build();
    try {
      if (cameraProvider.hasCamera(newCameraSelector)) {
        Log.d(TAG, "Set facing to " + newLensFacing);
        lensFacing = newLensFacing;
        cameraSelector = newCameraSelector;
        bindAllCameraUseCases();
        return;
      }
    } catch (CameraInfoUnavailableException e) {
      FirebaseCrashlytics.getInstance().recordException(e);
    }
    Toast.makeText(
            getApplicationContext(),
            "This device does not have lens with facing: " + newLensFacing,
            Toast.LENGTH_SHORT)
            .show();
  }

  @Override
  public void onResume() {
    super.onResume();
    bindAllCameraUseCases();
    checkLogin();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (imageProcessor != null) {
      imageProcessor.stop();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (imageProcessor != null) {
      imageProcessor.stop();
    }
  }

  private void bindAllCameraUseCases() {
    if (cameraProvider != null) {
      // As required by CameraX API, unbinds all use cases before trying to re-bind any of them.
      cameraProvider.unbindAll();
      bindPreviewUseCase();
      bindAnalysisUseCase();
    }
  }

  private void bindPreviewUseCase() {
    if (!PreferenceUtils.isCameraLiveViewportEnabled(this)) {
      return;
    }
    if (cameraProvider == null) {
      return;
    }
    if (previewUseCase != null) {
      cameraProvider.unbind(previewUseCase);
    }
    Size videoResolution = new Size(600, 800);
    Preview.Builder builder = new Preview.Builder();
    Size targetResolution = PreferenceUtils.getCameraXTargetResolution(this, lensFacing);
    if (targetResolution != null) builder.setTargetResolution(targetResolution);
    else builder.setTargetResolution(videoResolution);
    previewUseCase = builder.build();
    previewUseCase.setSurfaceProvider(previewView.getSurfaceProvider());
    cameraProvider.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector, previewUseCase);
  }

  @SuppressLint("RestrictedApi")
  private void bindAnalysisUseCase() {
    if (cameraProvider == null) {
      return;
    }
    if (analysisUseCase != null) {
      cameraProvider.unbind(analysisUseCase);
    }
    if (imageProcessor != null) {
      imageProcessor.stop();
    }

    try {
      PoseDetectorOptionsBase poseDetectorOptions =
              PreferenceUtils.getPoseDetectorOptionsForLivePreview(this);
      boolean shouldShowInFrameLikelihood =
              PreferenceUtils.shouldShowPoseDetectionInFrameLikelihoodLivePreview(this);
      boolean visualizeZ = PreferenceUtils.shouldPoseDetectionVisualizeZ(this);
      boolean rescaleZ = PreferenceUtils.shouldPoseDetectionRescaleZForVisualization(this);
      boolean runClassification = true;
      imageProcessor =
              new PoseDetectorProcessor(
                      this, poseDetectorOptions, shouldShowInFrameLikelihood, visualizeZ, rescaleZ,
                      runClassification, /* isStreamMode = */true);
    } catch (Exception e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      Log.e(TAG, "Can not create image processor: " + selectedModel, e);
      Toast.makeText(
              getApplicationContext(),
              "Can not create image processor: " + e.getLocalizedMessage(),
              Toast.LENGTH_LONG)
              .show();
      return;
    }

    ImageAnalysis.Builder builder = new ImageAnalysis.Builder();
    Size videoResolution = new Size(600, 800);
    Size targetResolution = PreferenceUtils.getCameraXTargetResolution(this, lensFacing);
    if (targetResolution != null) builder.setTargetResolution(targetResolution);
    else builder.setTargetResolution(videoResolution);
    analysisUseCase = builder.build();

    needUpdateGraphicOverlayImageSourceInfo = true;
    analysisUseCase.setAnalyzer(
        // imageProcessor.processImageProxy will use another thread to run the detection underneath,
        // thus we can just runs the analyzer itself on main thread.
        ContextCompat.getMainExecutor(this),
        imageProxy -> {
          if (needUpdateGraphicOverlayImageSourceInfo) {
            boolean isImageFlipped = lensFacing == CameraSelector.LENS_FACING_FRONT;
            int rotationDegrees = imageProxy.getImageInfo().getRotationDegrees();
            if (rotationDegrees == 0 || rotationDegrees == 180) {
              graphicOverlay.setImageSourceInfo(
                  imageProxy.getWidth(), imageProxy.getHeight(), isImageFlipped);
            } else {
              graphicOverlay.setImageSourceInfo(
                  imageProxy.getHeight(), imageProxy.getWidth(), isImageFlipped);
            }
            needUpdateGraphicOverlayImageSourceInfo = false;
          }
          try {
            imageProcessor.processImageProxy(imageProxy, graphicOverlay);
          } catch (MlKitException e) {
            FirebaseCrashlytics.getInstance().recordException(e);
            Log.e(TAG, "Failed to process image. Error: " + e.getLocalizedMessage());
            Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT)
                .show();
          }
        });
    VideoCapture.Builder videoBuilder = new VideoCapture.Builder();
    if (targetResolution != null) videoBuilder.setTargetResolution(targetResolution);
    else videoBuilder.setTargetResolution(videoResolution);
    videoCapture = videoBuilder.build();

    cameraProvider.bindToLifecycle(/* lifecycleOwner= */ this, cameraSelector, analysisUseCase, videoCapture);
  }

  private void updateUserRecord(UserModel userInfo, FirebaseUser user, FirebaseFirestore database) {
    database.collection("users").document(user.getUid()).set(userInfo);
    Map<String, Object> webSavedData = new HashMap<>();
    webSavedData.put("watched", new ArrayList<>());
    webSavedData.put("starred", new ArrayList<>());
    database.collection("users").document(user.getUid())
            .collection("data").document("saved_web_info")
            .set(webSavedData);
  }

  private void startRecordingCycle() {
    startRecording();
    cycleTimer = new CountDownTimer(60000, 1000) {
      @Override
      public void onTick(long millisUntilFinished) {

      }

      @Override
      public void onFinish() {
        stopRecording();
        cycleTimer = null;
      }
    }.start();
  }

  private void stopRecordingCycle() {
    isRecording = false;
    if (cycleTimer != null) {
      cycleTimer.cancel();
    }
    stopRecording();
    cycleTimer = null;

  }

  private void addUploadQue(UploadTaskModel event) {
    if (uploadQue == null) uploadQue = new ArrayList<UploadTaskModel>();
    uploadQue.add(event);

    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    SharedPreferences.Editor editor = prefs.edit();
    try {
      editor.putString(getString(R.string.pref_upload_que), ObjectSerializer.serialize(uploadQue));
    } catch (IOException e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      e.printStackTrace();
    }
    editor.apply();
  }

  private void updateUploadQue() {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    SharedPreferences.Editor editor = prefs.edit();
    try {
      editor.putString(getString(R.string.pref_upload_que), ObjectSerializer.serialize(uploadQue));
    } catch (IOException e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      e.printStackTrace();
    }
    editor.apply();
  }

  public static boolean isNetworkAvailable(Context context) {
    ConnectivityManager connectivityManager
            = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
  }

  @SuppressLint("RestrictedApi")
  private void startRecording() {
    videoIsBusy = true;

    File fileDir = new File(Environment.getExternalStorageDirectory(), folder_main);
    if (!fileDir.exists()) {
      if (!fileDir.mkdirs())
        Log.d(TAG, "suka, no file!");
    }


    long time = System.currentTimeMillis();

    File jsonFile = new File(fileDir, new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.US
    ).format(time) + "-data.json");


    File videoFile = new File(fileDir, new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.US
    ).format(time) + ".mp4");


    VideoCapture.OutputFileOptions outputOptions = new VideoCapture.OutputFileOptions.Builder(videoFile).build();

    imageProcessor.startDataRecorder(jsonFile, System.currentTimeMillis(), user, license_num);

    videoCapture.startRecording(outputOptions, ContextCompat.getMainExecutor(this), new VideoCapture.OnVideoSavedCallback() {
      @Override
      public void onVideoSaved(@NonNull VideoCapture.OutputFileResults outputFileResults) {
        Uri videoUri = Uri.fromFile(videoFile);
        Uri jsonUri = Uri.fromFile(jsonFile);

        String msg = "Сохранен видоефрагмент: " + videoUri.toString();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT)
                .show();

        Log.i(TAG, "Video capture success: " + videoUri);

        boolean hadEvents = imageProcessor.stopDataRecorder();
        if (hadEvents) {

          addUploadQue(new UploadTaskModel(videoUri, true));
          addUploadQue(new UploadTaskModel(jsonUri, false));

          if (isNetworkAvailable(getApplicationContext())) processUploads();
        }
        if (isRecording)
          startRecordingCycle();
      }

      @Override
      public void onError(int videoCaptureError, @NonNull String message, @Nullable Throwable cause) {
        videoIsBusy = false;
        Log.e(TAG, "Video capture failed: " + message);
        FirebaseCrashlytics.getInstance().log("Video capture failed: " + message);
      }
    });

    startStopRec.setText(R.string.recordingStop);
    startStopRec.setBackgroundColor(getColor(R.color.red));

  }

  private void processUploads() {
    for (Iterator<UploadTaskModel> itr = uploadQue.iterator(); itr.hasNext();) {
      UploadTaskModel uploadEvent = itr.next();
      itr.remove();
      if (uploadEvent.isVideo) {
        if (uploadEvent.videoSessionUri != null) {
          sendDataToStorage(uploadEvent, uploadEvent.videoSessionUri);
        } else {
          sendDataToStorage(uploadEvent);
        }
      } else {
        sendEventDataToFireStore(uploadEvent);
      }
    }
  }

  private void sendEventDataToFireStore(UploadTaskModel event) {
    Uri jsonUri = event.jsonUri;
    UploadTaskModel eventWithSession = new UploadTaskModel(jsonUri, false);
    updateUploadQue();

    Gson gson = new Gson();
    StringBuilder text = new StringBuilder();
    String path = jsonUri.getPath();

    try {
      BufferedReader br = new BufferedReader(new FileReader(new File(path)));
      String line;

      while ((line = br.readLine()) != null) {
        text.append(line);
        text.append('\n');
      }
      br.close();
    }
    catch (IOException e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      Toast.makeText(getApplicationContext(), "нет файла с данными о нарушениях", Toast.LENGTH_SHORT)
              .show();
      Log.e(TAG, e.getMessage());
    }

    String json = text.toString();
    EventModel eventModel = gson.fromJson(json, EventModel.class);

    String videoName = jsonUri.getLastPathSegment().replace("-data.json",".mp4");
    firestoredb.collection("users").document(user.getUid())
            .collection("videos").document(videoName).set(eventModel)
            .addOnFailureListener(e -> {
              addUploadQue(eventWithSession);
              Log.e(TAG, e.getMessage());
            })
            .addOnCompleteListener(task -> {
              Toast.makeText(getApplicationContext(), "Данные успешно загружены в облако", Toast.LENGTH_SHORT)
                      .show();

            });
  }

  private void sendDataToStorage(UploadTaskModel event) {
    UploadTaskModel eventWithSession = new UploadTaskModel(event.videoUri, true);
    updateUploadQue();

    StorageReference storageRef = storage.getReference();


    UploadTask videoUploadTask = storageRef.child("videos/" + user.getUid() + "/" + event.videoUri.getLastPathSegment()).putFile(event.videoUri);


    videoUploadTask.addOnProgressListener(taskSnapshot -> {
      double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
      Log.d(TAG, "Video Upload is " + progress + "% done");
      eventWithSession.videoSessionUri = taskSnapshot.getUploadSessionUri();
    }).addOnPausedListener(taskSnapshot -> Log.d(TAG, "Video Upload is paused")).addOnFailureListener(new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception exception) {
        addUploadQue(eventWithSession);
        Log.e(TAG, exception.getMessage());
      }
    }).addOnSuccessListener(taskSnapshot -> Toast.makeText(getApplicationContext(), "Видео успешно загружено в облако", Toast.LENGTH_SHORT)
            .show());
  }

  private void sendDataToStorage(UploadTaskModel event, Uri videoSession) {
    UploadTaskModel eventWithSession = new UploadTaskModel(event.videoUri, true);
    updateUploadQue();

    StorageReference storageRef = storage.getReference();
    StorageMetadata videoMetadata = new StorageMetadata.Builder()
            .build();

    UploadTask videoUploadTask = storageRef.child("videos/"+user.getUid()+"/"+event.videoUri.getLastPathSegment()).putFile(event.videoUri, videoMetadata, videoSession);

    videoUploadTask.addOnProgressListener(taskSnapshot -> {
      double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
      Log.d(TAG, "Video Upload is " + progress + "% done");
      eventWithSession.videoSessionUri = taskSnapshot.getUploadSessionUri();
    }).addOnPausedListener(taskSnapshot -> Log.d(TAG, "Video Upload is paused")).addOnFailureListener(new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception exception) {
        addUploadQue(eventWithSession);
        Log.e(TAG, exception.getMessage());
      }
    }).addOnSuccessListener(taskSnapshot -> Toast.makeText(getApplicationContext(), "Видео успешно загружено в облако", Toast.LENGTH_SHORT)
            .show());

  }


  @SuppressLint("RestrictedApi")
  private void stopRecording() {
    videoCapture.stopRecording();
  }

  private String[] getRequiredPermissions() {
    try {
      PackageInfo info =
          this.getPackageManager()
              .getPackageInfo(this.getPackageName(), PackageManager.GET_PERMISSIONS);
      String[] ps = info.requestedPermissions;
      if (ps != null && ps.length > 0) {
        return ps;
      } else {
        return new String[0];
      }
    } catch (Exception e) {
      FirebaseCrashlytics.getInstance().recordException(e);
      return new String[0];
    }
  }

  private boolean allPermissionsGranted() {
    for (String permission : getRequiredPermissions()) {
      if (!isPermissionGranted(this, permission)) {
        return false;
      }
    }
    return true;
  }

  private void getRuntimePermissions() {
    List<String> allNeededPermissions = new ArrayList<>();
    for (String permission : getRequiredPermissions()) {
      if (!isPermissionGranted(this, permission)) {
        allNeededPermissions.add(permission);
      }
    }

    if (!allNeededPermissions.isEmpty()) {
      ActivityCompat.requestPermissions(
          this, allNeededPermissions.toArray(new String[0]), PERMISSION_REQUESTS);
    }
  }

  @Override
  public void onRequestPermissionsResult(
      int requestCode, String[] permissions, int[] grantResults) {
    Log.i(TAG, "Permission granted!");
    if (allPermissionsGranted()) {
      bindAllCameraUseCases();
    }
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
  }

  private static boolean isPermissionGranted(Context context, String permission) {
    if (ContextCompat.checkSelfPermission(context, permission)
        == PackageManager.PERMISSION_GRANTED) {
      Log.i(TAG, "Permission granted: " + permission);
      return true;
    }
    Log.i(TAG, "Permission NOT granted: " + permission);
    return false;
  }
}
