package com.google.mlkit.vision.driveDistraction.java.models;

public class UserModel {
    public String username;
    public String email;
    public String company;
    public String phone;
    public boolean isAdmin;

    public UserModel() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public UserModel(String username, String email, String phone, String company, boolean isAdmin) {
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.company = company;
        this.isAdmin = isAdmin;
    }
}
