/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.mlkit.vision.driveDistraction.java.posedetector.classification;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import androidx.annotation.WorkerThread;
import com.google.common.base.Preconditions;
import com.google.mlkit.vision.driveDistraction.R;
import com.google.mlkit.vision.driveDistraction.java.posedetector.PoseDetectorProcessor;
import com.google.mlkit.vision.driveDistraction.preference.PreferenceUtils;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseLandmark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.atan2;

/**
 * Accepts a stream of {@link Pose} for classification and Rep counting.
 */
public class PoseClassifierProcessor {
  private static final String TAG = "PoseClassifierProcessor";
  private static final String POSE_SAMPLES_FILE = "pose/driver_poses_csvs_out.csv";

  // Specify classes for which we want rep counting.
  // These are the labels in the given {@code POSE_SAMPLES_FILE}. You can set your own class labels
  // for your pose samples.
  private static final String RUKA_SLEVA = "0-Ruka sleva (SMS, eda etc.)";
  //private static final String SQUATS_CLASS = "squats_down";
  private static final String[] POSE_CLASSES = {
          RUKA_SLEVA
  };

  private final boolean isStreamMode;

  private EMASmoothing emaSmoothing;
  private List<RepetitionCounter> repCounters;
  private PoseClassifier poseClassifier;
  private String lastRepResult;
  List<String> alertText = new ArrayList<>();
  private Context context;
  PoseDetectorProcessor.ClassificationResultData result;

  private Long lastActiveStampFace;
  private Long lastInactiveStampFace;
  private boolean inactiveIntervalFace = false;
  private boolean armFace = false;

  private Long lastActiveStampReaching;
  private Long lastInactiveStampReaching;
  private boolean inactiveIntervalReaching = false;
  private boolean armReaching = false;


  private float baseArmToFace;
  private float baseArmToHips;

  private float nosethreshold;
  private float hipsthreshold;

  private float noseMinAngleThreshold;
  private float reachingMaxAngleThreshold;

  private float minDistanceArmToHip;
  private float minDistanceNoseToWrist;



  private Long lastActiveStampHips;
  private Long lastInactiveStampHips;
  private boolean inactiveIntervalHips = false;
  private boolean armHips = false;

  @WorkerThread
  public PoseClassifierProcessor(Context context, boolean isStreamMode) {
    Preconditions.checkState(Looper.myLooper() != Looper.getMainLooper());
    this.isStreamMode = isStreamMode;
    this.context = context;

    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    baseArmToHips = sharedPreferences.getFloat(context.getString(R.string.pref_base_armToHips), 190.0f);
    baseArmToFace = sharedPreferences.getFloat(context.getString(R.string.pref_base_armToNose), 190.0f);

    if (isStreamMode) {
      emaSmoothing = new EMASmoothing();
      repCounters = new ArrayList<>();
      lastRepResult = "";
    }
  }

  private void loadPoseSamples(Context context) {

    List<PoseSample> poseSamples = new ArrayList<>();
    try {
      BufferedReader reader = new BufferedReader(
              new InputStreamReader(context.getAssets().open(POSE_SAMPLES_FILE)));
      String csvLine = reader.readLine();
      while (csvLine != null) {
        // If line is not a valid {@link PoseSample}, we'll get null and skip adding to the list.
        PoseSample poseSample = PoseSample.getPoseSample(csvLine, ",");
        if (poseSample != null) {
          poseSamples.add(poseSample);
        }
        csvLine = reader.readLine();
      }
    } catch (IOException e) {
      Log.e(TAG, "Error when loading pose samples.\n" + e);
    }
    poseClassifier = new PoseClassifier(poseSamples);
    if (isStreamMode) {
      for (String className : POSE_CLASSES) {
        repCounters.add(new RepetitionCounter(className));
      }
    }
  }

  public void setupBaseValues() {
    baseArmToFace = minDistanceNoseToWrist;
    baseArmToHips = minDistanceArmToHip;

    PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putFloat(context.getString(R.string.pref_base_armToHips),baseArmToHips)
            .putFloat(context.getString(R.string.pref_base_armToNose), baseArmToFace)
            .apply();

  }


  static double getDistance(PoseLandmark firstPoint, PoseLandmark secondPoint) {
    return Math.sqrt(
            ((secondPoint.getPosition().x - firstPoint.getPosition().x)
                    * (secondPoint.getPosition().x - firstPoint.getPosition().x))
                    + ((secondPoint.getPosition().y-firstPoint.getPosition().y)
                    * (secondPoint.getPosition().y-firstPoint.getPosition().y)));
  }

  static double getAngle(PoseLandmark firstPoint, PoseLandmark midPoint, PoseLandmark lastPoint) {
    double result =
            Math.toDegrees(
                    atan2(lastPoint.getPosition().y - midPoint.getPosition().y,
                            lastPoint.getPosition().x - midPoint.getPosition().x)
                            - atan2(firstPoint.getPosition().y - midPoint.getPosition().y,
                            firstPoint.getPosition().x - midPoint.getPosition().x));
    result = Math.abs(result); // Angle should never be negative
    if (result > 180) {
      result = (360.0 - result); // Always get the acute representation of the angle
    }
    return result;
  }


  void alertGenerateNose(double leftAngle, double rightAngle, double minDistanceNoseToWrist) {
    float minAngle = (float) Math.min(leftAngle, rightAngle);
    long curTimeStamp = System.currentTimeMillis();
    float threshold = baseArmToFace * (1 - nosethreshold);
    if (minDistanceNoseToWrist < threshold && (!armFace || inactiveIntervalFace)) {
      lastActiveStampFace = curTimeStamp;
      inactiveIntervalFace = false;
      armFace = true;
    } else if (minDistanceNoseToWrist > threshold && armFace) {
      lastInactiveStampFace = curTimeStamp;
      inactiveIntervalFace = true;
    }
    if (lastInactiveStampFace!=null) {
      if (lastInactiveStampFace - lastActiveStampFace > 500 && inactiveIntervalFace) {
        armFace = false;
        inactiveIntervalFace = false;
        if (alertText.remove("Руки у лица!")) {
          if (result.events == null) result.events = new ArrayList<>();
          result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                  PoseDetectorProcessor.State.END, "Руки у лица!")
          );
        }
      }
    }
    if (lastActiveStampFace!=null) {
      if (curTimeStamp - lastActiveStampFace > 1500 && armFace) {
        if(minAngle < 60.0f)
          if(!alertText.contains("Руки у лица!")) {
            alertText.add("Руки у лица!");
            if (result.events == null) result.events = new ArrayList<>();
            result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                    PoseDetectorProcessor.State.START, "Руки у лица!")
            );
          }
      }
    }

  }


  void alertGenerateHip(double leftAngle, double rightAngle, double minDistanceArmToHip) {
    long curTimeStamp = System.currentTimeMillis();
    float threshold = baseArmToHips * (1 - hipsthreshold);
    if (minDistanceArmToHip < threshold && (!armHips || inactiveIntervalHips)) {
      lastActiveStampHips = curTimeStamp;
      inactiveIntervalHips = false;
      armHips = true;
    } else if (minDistanceArmToHip > threshold && armHips) {
      lastInactiveStampHips = curTimeStamp;
      inactiveIntervalHips = true;
    }
    if (lastInactiveStampHips!=null) {
      if (lastInactiveStampHips - lastActiveStampHips > 500 && inactiveIntervalHips) {
        armHips = false;
        inactiveIntervalHips = false;
        if (alertText.remove("Руки снизу!")) {
          if (result.events == null) result.events = new ArrayList<>();
          result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                  PoseDetectorProcessor.State.END, "Руки снизу!")
          );
        }
      }
    }
    if (lastActiveStampHips!=null) {
      if (curTimeStamp - lastActiveStampHips > 1500 && armHips) {
        if (!alertText.contains("Руки снизу!")) {
          alertText.add("Руки снизу!");
          if (result.events == null) result.events = new ArrayList<>();
          result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                  PoseDetectorProcessor.State.START, "Руки снизу!")
          );
        }
      }
    }

  }

  void alertGenerateReaching(double angle) {
    long curTimeStamp = System.currentTimeMillis();
    float threshold = 130.0f;
    if (angle > threshold && (!armReaching || inactiveIntervalReaching)) {
      lastActiveStampReaching = curTimeStamp;
      inactiveIntervalReaching = false;
      armReaching = true;
    } else if (angle < threshold && armReaching) {
      lastInactiveStampReaching = curTimeStamp;
      inactiveIntervalReaching = true;
    }
    if (lastInactiveStampReaching!=null) {
      if (lastInactiveStampReaching - lastActiveStampReaching > 500 && inactiveIntervalReaching) {
        armReaching = false;
        inactiveIntervalReaching = false;
        if (alertText.remove("Руки далеко!")) {
          if (result.events == null) result.events = new ArrayList<>();
          result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                  PoseDetectorProcessor.State.END, "Руки далеко!")
          );
        }
      }
    }
    if (lastActiveStampReaching!=null) {
      if (curTimeStamp - lastActiveStampReaching > 1500 && armReaching) {
        if (!alertText.contains("Руки далеко!")) {
          alertText.add("Руки далеко!");
          if (result.events == null) result.events = new ArrayList<>();
          result.events.add(new PoseDetectorProcessor.ClassificationResultData.Event(
                  PoseDetectorProcessor.State.START, "Руки далеко!")
          );
        }

      }
    }
  }


  /**
   * Given a new {@link Pose} input, returns a list of formatted {@link String}s with Pose
   * classification results.
   *
   * <p>Currently it returns up to 2 strings as following:
   * 0: PoseClass : X reps
   * 1: PoseClass : [0.0-1.0] confidence
   */
  @WorkerThread
  public PoseDetectorProcessor.ClassificationResultData getPoseResult(Pose pose) {
    Preconditions.checkState(Looper.myLooper() != Looper.getMainLooper());
    result = new PoseDetectorProcessor.ClassificationResultData(new ArrayList<>(), "", new ArrayList<>());
    // ClassificationResult classification = poseClassifier.classify(pose);

    // Update {@link RepetitionCounter}s if {@code isStreamMode}.
    if (isStreamMode) {
      // Feed pose to smoothing even if no pose found.
      //classification = emaSmoothing.getSmoothedResult(classification);

      // Return early without updating repCounter if no pose found.
      if (pose.getAllPoseLandmarks().isEmpty()) {
        result.metadata.add(lastRepResult);
        return result;
      }

//      for (RepetitionCounter repCounter : repCounters) {
//        int repsBefore = repCounter.getNumRepeats();
//        int repsAfter = repCounter.addClassificationResult(classification);
//        if (repsAfter > repsBefore) {
//          // Play a fun beep when rep counter updates.
//          ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
//          tg.startTone(ToneGenerator.TONE_PROP_BEEP);
//          lastRepResult = String.format(
//              Locale.US, "%s : %d reps", repCounter.getClassName(), repsAfter);
//          break;
//        }
//      }
//      result.add(lastRepResult);
    }

    // Add maxConfidence class of current frame to result if pose is found.
    if (!pose.getAllPoseLandmarks().isEmpty()) {

      PoseLandmark rightWrist = pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST);
      PoseLandmark leftWrist = pose.getPoseLandmark(PoseLandmark.LEFT_WRIST);
      PoseLandmark nose = pose.getPoseLandmark(PoseLandmark.NOSE);
      PoseLandmark rightHip =pose.getPoseLandmark(PoseLandmark.RIGHT_HIP);
      PoseLandmark leftHip =pose.getPoseLandmark(PoseLandmark.LEFT_HIP);

      double leftElbowAnge = getAngle(
              pose.getPoseLandmark(PoseLandmark.LEFT_WRIST),
              pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW),
              pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER)
      );

      double leftArmChestAngle = getAngle(
              pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER),
              pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER),
              pose.getPoseLandmark(PoseLandmark.LEFT_ELBOW)
      );

      double rightElbowAnge = getAngle(
              pose.getPoseLandmark(PoseLandmark.RIGHT_WRIST),
              pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW),
              pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER)
      );

      double rightArmChestAngle = getAngle(
              pose.getPoseLandmark(PoseLandmark.LEFT_SHOULDER),
              pose.getPoseLandmark(PoseLandmark.RIGHT_SHOULDER),
              pose.getPoseLandmark(PoseLandmark.RIGHT_ELBOW)
      );

      double maxArmChestAngle = Math.max(leftArmChestAngle, rightArmChestAngle);

      String rightAngle = String.format(
              Locale.US,
              "right elbow : %.2f",
              rightElbowAnge);

      String leftAngle = String.format(
              Locale.US,
              "right elbow : %.2f",
              leftElbowAnge);
      result.metadata.add(rightAngle);
      result.metadata.add(leftAngle);


//      String maxConfidenceClass = classification.getMaxConfidenceClass();
//      String maxConfidenceClassResult = String.format(
//              Locale.US,
//              "%s : %.2f confidence",
//              maxConfidenceClass,
//              classification.getClassConfidence(maxConfidenceClass)
//                      / poseClassifier.confidenceRange());
      if (rightWrist!=null&&leftWrist!=null&&nose!=null) {

        double rightNoseToWristDist = getDistance(rightWrist,nose);
        double leftNoseToWristDist = getDistance(leftWrist,nose);
        minDistanceNoseToWrist = (float) Math.min(rightNoseToWristDist,leftNoseToWristDist);

        double leftWristToHipDist = getDistance(leftWrist, leftHip);
        double rightWristToHipDist = getDistance(rightWrist, rightHip);
        minDistanceArmToHip = (float) Math.min(rightWristToHipDist,leftWristToHipDist);

        String minDistanceToNose = String.format(
                Locale.US,
                "Nose : %.2f",
                minDistanceNoseToWrist);
        String minDistanceToHip = String.format(
                Locale.US,
                "Hip : %.2f",
                minDistanceArmToHip);
        result.metadata.add(minDistanceToNose);
        result.metadata.add(minDistanceToHip);
        alertGenerateNose(leftElbowAnge, rightElbowAnge, minDistanceNoseToWrist);
        alertGenerateHip(leftElbowAnge, rightElbowAnge, minDistanceArmToHip);
        alertGenerateReaching(maxArmChestAngle);
      }


      result.alertText = String.join(", ", alertText);
    }

    return result;
  }


}
