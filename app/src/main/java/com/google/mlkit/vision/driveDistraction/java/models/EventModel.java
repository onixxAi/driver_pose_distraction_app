package com.google.mlkit.vision.driveDistraction.java.models;

import java.util.ArrayList;

public class EventModel {
    public ArrayList<String> summary;
    public Metadata metadata;
    public ArrayList<Event> events;

    public EventModel() {

    }

    public EventModel(ArrayList<String> summary, Metadata metadata, ArrayList<Event> events) {
        this.summary = summary;
        this.metadata = metadata;
        this.events = events;
    }

    public static class Metadata {
        public String name;
        public String licensePlate;
        public boolean reviewedByManager;
        public boolean reviewedByAdmin;
        public boolean approvedByAdmin;

        public Metadata(String name, String licensePlate) {
            this.name = name;
            this.licensePlate = licensePlate;
            this.reviewedByManager = false;
            this.approvedByAdmin = false;
            this.reviewedByAdmin = false;
        }
    }

    public static class Event {
        public long startTime;
        public long endTime;
        public String eventName;
        public boolean isWrongDetection;

        public Event(long startTime, long endTime, String eventName) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.eventName = eventName;
            this.isWrongDetection = false;

        }
    }
}
