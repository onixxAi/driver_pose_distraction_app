/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.mlkit.vision.driveDistraction.java.posedetector;

import android.content.Context;
import androidx.annotation.NonNull;

import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.driveDistraction.GraphicOverlay;
import com.google.mlkit.vision.driveDistraction.java.VisionProcessorBase;
import com.google.mlkit.vision.driveDistraction.java.models.EventModel;
import com.google.mlkit.vision.driveDistraction.java.posedetector.classification.PoseClassifierProcessor;
import com.google.mlkit.vision.pose.Pose;
import com.google.mlkit.vision.pose.PoseDetection;
import com.google.mlkit.vision.pose.PoseDetector;
import com.google.mlkit.vision.pose.PoseDetectorOptionsBase;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/** A processor to run pose detector. */
public class PoseDetectorProcessor
    extends VisionProcessorBase<PoseDetectorProcessor.PoseWithClassification> {
  private static final String TAG = "PoseDetectorProcessor";

  private final PoseDetector detector;

  private final boolean showInFrameLikelihood;
  private final boolean visualizeZ;
  private final boolean rescaleZForVisualization;
  private final boolean runClassification;
  private final boolean isStreamMode;
  private final Context context;
  private final Executor classificationExecutor;

  private PoseClassifierProcessor poseClassifierProcessor;

  private long startTime;
  private FirebaseUser firebaseUser;
  private EventModel eventModel;
  private ArrayList<EventModel.Event> eventArrayList;
  private ArrayList<String> summary;
  private File saveFile;
  /**
   * Internal class to hold Pose and classification results.
   */

  public enum State{
    START, END
  }

  public static class ClassificationResultData {
    public List<String> metadata;
    public String alertText;
    public ArrayList<Event> events;

    public ClassificationResultData(List<String> metadata, String alerts, ArrayList<Event> events) {
      this.metadata = metadata;
      this.alertText = alerts;
      this.events = events;
    }

    public static class Event {
      public State eventState;
      public String eventName;

      public Event(State eventState, String eventName) {
        this.eventState = eventState;
        this.eventName = eventName;
      }
    }
  }
  protected static class PoseWithClassification {
    private final Pose pose;
    private final ClassificationResultData classificationResultData;
    private final List<String> classificationResult;
    private final String alertText;




    public PoseWithClassification(Pose pose, ClassificationResultData classificationResult) {
      this.pose = pose;
      this.classificationResultData = classificationResult;
      this.classificationResult = classificationResultData.metadata;
      this.alertText = classificationResult.alertText;
    }

    public Pose getPose() {
      return pose;
    }

    public List<String> getClassificationResult() {
      return classificationResult;
    }
  }

  public PoseDetectorProcessor(
      Context context,
      PoseDetectorOptionsBase options,
      boolean showInFrameLikelihood,
      boolean visualizeZ,
      boolean rescaleZForVisualization,
      boolean runClassification,
      boolean isStreamMode) {
    super(context);
    this.showInFrameLikelihood = showInFrameLikelihood;
    this.visualizeZ = visualizeZ;
    this.rescaleZForVisualization = rescaleZForVisualization;
    detector = PoseDetection.getClient(options);
    this.runClassification = runClassification;
    this.isStreamMode = isStreamMode;
    this.context = context;
    classificationExecutor = Executors.newSingleThreadExecutor();
  }

  @Override
  public void stop() {
    super.stop();
    detector.close();
  }

  @Override
  protected Task<PoseWithClassification> detectInImage(InputImage image) {
    return detector
        .process(image)
        .continueWith(
            classificationExecutor,
            task -> {
              Pose pose = task.getResult();
              ClassificationResultData classificationResult = new ClassificationResultData(new ArrayList<>(), "", new ArrayList<>());
              if (runClassification) {
                if (poseClassifierProcessor == null) {
                  poseClassifierProcessor = new PoseClassifierProcessor(context, isStreamMode);
                }
                classificationResult = poseClassifierProcessor.getPoseResult(pose);
              }
              return new PoseWithClassification(pose, classificationResult);
            });
  }

  @Override
  protected void setupBaseValues() {
    if (poseClassifierProcessor!=null)
      poseClassifierProcessor.setupBaseValues();
  }

  @Override
  protected void startJsonRecording(File file, long time, FirebaseUser user, String licensePlate) {
    summary = new ArrayList<>();
    eventArrayList = new ArrayList<>();
    eventModel = new EventModel(summary, new EventModel.Metadata(user.getDisplayName(), licensePlate), eventArrayList);
    saveFile = file;
    startTime = time;
    firebaseUser = user;
  }

  @Override
  protected boolean saveJsonRecording() {
    if (eventArrayList.size()>0) {
      eventModel.events = eventArrayList;

      Writer output = null;
      try {
        Gson gson = new Gson();
        String json = gson.toJson(eventModel);
        output = new BufferedWriter(new FileWriter(saveFile));
        output.write(json);
        output.close();
        eventModel = null;
        eventArrayList = null;
        return true;
      } catch (IOException e) {
        Log.d(TAG, "Error while saving file: " + e.getMessage());
        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT)
                .show();
        return false;
      }
    } else {
      Toast.makeText(context, "Нет проишествий за данную минуту", Toast.LENGTH_SHORT)
              .show();
      eventModel = null;
      eventArrayList = null;
      return false;
    }

  }


  void putEventDataToArray(PoseWithClassification result) {
    long timeInVideo = System.currentTimeMillis() - startTime;
    ArrayList<ClassificationResultData.Event> events = result.classificationResultData.events;
    if (events.size()>0) {
      for (ClassificationResultData.Event event: events) {
        if (event.eventState == State.START) {
          if (!checkForExistingStartedEvent(event.eventName))
            eventArrayList.add(new EventModel.Event(timeInVideo, -1, event.eventName));
        } else {
          replaceExistingEvent(event.eventName, timeInVideo);
        }
      }

    }


  }

  private void replaceExistingEvent(String eventName, long time) {
    for (EventModel.Event event: eventArrayList) {
      if (event.eventName.equals(eventName) && event.endTime==-1) {
        eventArrayList.set(eventArrayList.indexOf(event), new EventModel.Event(event.startTime, time, eventName));
        return;
      }
    }
  }

  private boolean checkForExistingStartedEvent(String eventName) {
    for (EventModel.Event event: eventArrayList) {
      if (event.eventName.equals(eventName) && event.endTime==-1) return true;
    }
    return false;
  }

  @Override
  protected void onSuccess(
      @NonNull PoseWithClassification poseWithClassification,
      @NonNull GraphicOverlay graphicOverlay) {
    graphicOverlay.add(
        new PoseGraphic(
            graphicOverlay, poseWithClassification.pose, showInFrameLikelihood, visualizeZ,
            rescaleZForVisualization, poseWithClassification.classificationResult, poseWithClassification.alertText));
    if (eventModel != null)
      putEventDataToArray(poseWithClassification);
  }

  @Override
  protected void onFailure(@NonNull Exception e) {
    Log.e(TAG, "Pose detection failed!", e);
  }
}
